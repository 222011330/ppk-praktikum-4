Nama : Arnoldy Fatwa Rahmadin

NIM : 222011330

Kelas : 3SI3

Praktikum Pemrograman Platform Khusus Pertemuan 4


1. TAMPILAN POSTMAN
![postman](/Dokumentasi/PPK-Pertemuan4_1.png)

2. TAMPILAN JSON
![json](/Dokumentasi/PPK-Pertemuan4_2.png)

3. TAMPILAN SWAGGER DI LOCALHOST
![swagger](/Dokumentasi/PPK-Pertemuan4_3.png)

4. MENAMBAHKAN DATA MELALUI SWAGGER
![tambah](/Dokumentasi/PPK-Pertemuan4_4.png)

5. MENGUBAH DATA MELALUI SWAGGER
![ubah](/Dokumentasi/PPK-Pertemuan4_5.png)

6. MENGHAPUS DATA MELALUI SWAGGER
![hapus](/Dokumentasi/PPK-Pertemuan4_6.png)



